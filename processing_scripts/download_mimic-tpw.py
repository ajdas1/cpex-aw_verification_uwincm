"""
Ajda Savarin
University of Washington
Created: 29 July 2021
"""

from datetime import datetime

import os
import sys
from urllib import request, error


args = sys.argv

if len(args) > 1:
  date = args[1]
  date_dt = datetime.strptime(date, '%Y%m%d%H')
else:
  date_dt = datetime.now()



server = 'ftp://ftp.ssec.wisc.edu/pub/mtpw2/data/'

cwd = os.getcwd()
cwd_split = cwd.split('/')


saveDir = ('/').join(cwd_split[:-1]) + '/data/MIMIC-TPW/'


def download_mimicTPW(date_dt):
  filePath = server + date_dt.strftime('%Y%m/')
  fileName = 'comp' + date_dt.strftime('%Y%m%d.%H') + '0000.nc'
  
  getFile = filePath + fileName
  saveFile = saveDir+'MIMIC-TPW_'+date_dt.strftime('%Y%m%d%H')+'.nc'
  
  if os.path.isfile(saveFile):    
    print('... MIMIC-TPW image ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' already exists.')

  else:
    try:
      request.urlretrieve(getFile, saveFile)    
      print('... MIMIC-TPW image ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' downloaded successfully.')
    except (error.HTTPError):
      print('... MIMIC-TPW image ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' is currently not available.')
    except (error.URLError):
      print('... MIMIC-TPW image ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' is currently not available.')

download_mimicTPW(date_dt)
