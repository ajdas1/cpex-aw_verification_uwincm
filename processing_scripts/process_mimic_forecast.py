"""
Ajda Savarin
University of Washington
Created: 29 July 2021

1. Download MIMIC-TPW obs data if available.
2. Get observed fields.
3. Get model forecast fields and interpolate to observed grid.
4. Save model and obs data in single file
5. Remove original observed file to save space.
"""
from datetime import datetime, timedelta
from netCDF4 import Dataset
from scipy.interpolate import RectBivariateSpline

import numpy as np

import os
import subprocess
import sys

import wrf

args = sys.argv

if len(args) > 1:
  date = args[1]
  date_dt = datetime.strptime(date, '%Y%m%d%H')
else:
  date_dt = datetime.now()
  date = date_dt.strftime('%Y%m%d%H')


#date = '2021073012'
date_dt = datetime.strptime(date, '%Y%m%d%H')


cwd = os.getcwd()
cwd_split = cwd.split('/')

baseDir = ('/').join(cwd_split[:-1])


fl_download = baseDir + '/processing_scripts/download_mimic-tpw.py'
mimicDir = baseDir + '/data/MIMIC-TPW/'
fcstDir = '/home/orca3/bkerns/models/uwincm-cpex-aw/'
saveDir = baseDir + '/data/MIMIC-model/'
saveFl = saveDir + 'MIMIC-model_' + date_dt.strftime('%Y%m%d%H') + '.nc'

if os.path.isfile(saveFl):
  print(' ... Processed data already exists. No further processing.')
else:

  # # # 1. Download MIMIC-TPW obs data (if available and not yet here)
  subprocess.call(['python', fl_download, date])

  # # # 2. Get MIMIC data
  mimic_flIn = mimicDir + 'MIMIC-TPW_' + date_dt.strftime('%Y%m%d%H') + '.nc'

  if os.path.isfile(mimic_flIn):
    data = Dataset(mimic_flIn, 'r')
    lon_mimic = np.array(data['lonArr'])[374:659]
    lat_mimic = np.array(data['latArr'])[380:529]
    tpw_mimic = np.array(data['tpwGrid'])[380:529, 374:659]
    data.close()
  else: 
    sys.exit()

  # # # 3. Get model forecast fields and interpolate to observed grid.
  day0_fcstDir = fcstDir + date_dt.strftime('%Y%m%d' + '00/')
  day1_fcstDir = fcstDir + (date_dt-timedelta(days=1)).strftime('%Y%m%d' + '00/')
  day2_fcstDir = fcstDir + (date_dt-timedelta(days=2)).strftime('%Y%m%d' + '00/')

  # for day 0 forecast
  day0_flIn = day0_fcstDir + 'wrfout_d01_' + date_dt.strftime('%Y-%m-%d_%H:00:00')
  day1_flIn = day1_fcstDir + 'wrfout_d01_' + date_dt.strftime('%Y-%m-%d_%H:00:00')
  day2_flIn = day2_fcstDir + 'wrfout_d01_' + date_dt.strftime('%Y-%m-%d_%H:00:00')

  if os.path.isfile(day0_flIn):
    data = Dataset(day0_flIn, 'r')
    lon = wrf.getvar(data, 'lon')[0]
    lat = wrf.getvar(data, 'lat')[:, 0]
    tpw_day0_fcst = wrf.getvar(data, 'pw')
    data.close()
    intpTmp = RectBivariateSpline(lat, lon, tpw_day0_fcst)
    tpw_day0_mimic = intpTmp(lat_mimic, lon_mimic)
  else:
    tpw_day0_mimic = np.zeros((len(lat_mimic), len(lon_mimic))) * np.nan

  if os.path.isfile(day1_flIn):
    data = Dataset(day1_flIn, 'r')
    lon = wrf.getvar(data, 'lon')[0]
    lat = wrf.getvar(data, 'lat')[:, 0]
    tpw_day1_fcst = wrf.getvar(data, 'pw')
    data.close()
    intpTmp = RectBivariateSpline(lat, lon, tpw_day1_fcst)
    tpw_day1_mimic = intpTmp(lat_mimic, lon_mimic)
  else:
    tpw_day1_mimic = np.zeros((len(lat_mimic), len(lon_mimic))) * np.nan

  if os.path.isfile(day2_flIn):
    data = Dataset(day2_flIn, 'r')
    lon = wrf.getvar(data, 'lon')[0]
    lat = wrf.getvar(data, 'lat')[:, 0]
    tpw_day2_fcst = wrf.getvar(data, 'pw')
    data.close()
    intpTmp = RectBivariateSpline(lat, lon, tpw_day2_fcst)
    tpw_day2_mimic = intpTmp(lat_mimic, lon_mimic)
  else:
    tpw_day2_mimic = np.zeros((len(lat_mimic), len(lon_mimic))) * np.nan


  # # # 4. Save to combined netCDF file.


  outFl = Dataset(saveFl, 'w')

  outFl.createDimension('lon', len(lon_mimic))
  outFl.createDimension('lat', len(lat_mimic))

  v0 = outFl.createVariable('lon', 'f8', ('lon'))
  v0[:] = lon_mimic
  v0.units = 'degrees_east'

  v0 = outFl.createVariable('lat', 'f8', ('lat'))
  v0[:] = lat_mimic
  v0.units = 'degrees_north'

  v1 = outFl.createVariable('OBS', 'f8', ('lat', 'lon'))
  v1[:] = tpw_mimic
  v1.units = 'mm'

  v1 = outFl.createVariable('MOD_d0', 'f8', ('lat', 'lon'))
  v1[:] = tpw_day0_mimic
  v1.units = 'mm'

  v1 = outFl.createVariable('MOD_d1', 'f8', ('lat', 'lon'))
  v1[:] = tpw_day1_mimic
  v1.units = 'mm'


  v1 = outFl.createVariable('MOD_d2', 'f8', ('lat', 'lon'))
  v1[:] = tpw_day2_mimic
  v1.units = 'mm'

  outFl.close()
  print('... Wrote processed fields to ' + saveFl.split('/')[-1] + '.')


  # # # 5. Remove original observed file to save space.
  os.remove(mimic_flIn)
