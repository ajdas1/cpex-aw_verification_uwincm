"""
Ajda Savarin
University of Washington
Created: 6 August 2021

1. Download IMERG-early obs data if available.
2. Get observed fields.
3. Get model forecast fields and interpolate to observed grid.
4. Save model and obs data in single file
5. Remove original observed file to save space.
"""

from datetime import datetime, timedelta
from netCDF4 import Dataset
from scipy.interpolate import RectBivariateSpline
from scipy.interpolate import pchip

import numpy as np

import os
import subprocess
import sys

import wrf

args = sys.argv

if len(args) > 1:
  date = args[1]
  date_dt = datetime.strptime(date, '%Y%m%d%H')
else:
  date_dt = datetime.now()
  date = date_dt.strftime('%Y%m%d%H')


accumulation_hours = 3
accumulate_date = sorted([date_dt - timedelta(hours=hr) for hr in range(accumulation_hours)])


cwd = os.getcwd()
cwd_split = cwd.split('/')

baseDir = ('/').join(cwd_split[:-1])


fl_download = baseDir + '/processing_scripts/download_imerg-early.py'
imergDir = baseDir + '/data/IMERG-early/'
fcstDir = '/home/orca3/bkerns/models/uwincm-cpex-aw/'
saveDir = baseDir + '/data/IMERG-model/'
saveFl = saveDir + 'IMERG-model_' + date_dt.strftime('%Y%m%d%H') + '.nc'

if os.path.isfile(saveFl):
  print(' ... Processed data already exists. No further processing.')
else:
  # # # 1. Download IMERG-early obs data (if available and not yet here)
  for date in accumulate_date:
    subprocess.call(['python', fl_download, date.strftime('%Y%m%d%H')])

  # # # 2. Get observed fields
  imerg_flIn = [imergDir + 'IMERG-E_' + dt.strftime('%Y%m%d%H') + '.nc' for dt in accumulate_date]

  if not os.path.isfile(imerg_flIn[-1]):
    print(' ... Cannot process ' + date_dt.strftime('%Y%m%d%H') + ' as observations are not yet available.')
    sys.exit()
  else:
    data = Dataset(imerg_flIn[0], 'r')
    lon_imerg = np.array(data['lon'])[925:1646]
    lat_imerg = np.array(data['lat'])[949:1321]
    data.close()
  
    rain_imerg = []
    for fl in imerg_flIn:
      data = Dataset(fl, 'r')
      rain_imerg.append(np.array(data['precipitation'][949:1321, 925:1646]))
      data.close()
    rain_imerg = np.nansum(np.stack(rain_imerg), 0)
  

    # # # 3. Get model forecast fields and interpolate to observed grid.
    day0_fcstDir = fcstDir + date_dt.strftime('%Y%m%d' + '00/')
    day1_fcstDir = fcstDir + (date_dt-timedelta(days=1)).strftime('%Y%m%d' + '00/')
    day2_fcstDir = fcstDir + (date_dt-timedelta(days=2)).strftime('%Y%m%d' + '00/')

    # for day 0 forecast
    day0_flIn = sorted([day0_fcstDir + 'wrfout_d01_' + dt.strftime('%Y-%m-%d_%H:00:00') for dt in accumulate_date])
    day1_flIn = sorted([day1_fcstDir + 'wrfout_d01_' + dt.strftime('%Y-%m-%d_%H:00:00') for dt in accumulate_date])
    day2_flIn = sorted([day2_fcstDir + 'wrfout_d01_' + dt.strftime('%Y-%m-%d_%H:00:00') for dt in accumulate_date])

    day0_flIn = [fl for fl in day0_flIn if os.path.isfile(fl)]
    day1_flIn = [fl for fl in day1_flIn if os.path.isfile(fl)]
    day2_flIn = [fl for fl in day2_flIn if os.path.isfile(fl)]

    if len(day0_flIn) > 0:
      if date_dt.hour >= 3:
        data1, data2 = Dataset(day0_flIn[0], 'r'), Dataset(day0_flIn[-1], 'r')
        lon = wrf.getvar(data1, 'lon')[0]
        lat = wrf.getvar(data1, 'lat')[:, 0]
        rain_day0_fcst = (np.array(data2['RAINC'][0]) + np.array(data2['RAINNC'][0])) - (np.array(data1['RAINC'][0]) + np.array(data1['RAINNC'][0]))
        data1.close()
        data2.close()
        intpTmp = RectBivariateSpline(lat, lon, rain_day0_fcst, kx=1, ky=1)
        rain_day0_imerg = intpTmp(lat_imerg, lon_imerg)
    else:
      rain_day0_imerg = np.zeros((len(lat_imerg), len(lon_imerg))) * np.nan
    
    if len(day1_flIn) > 0:
      if os.path.isfile(day1_flIn[0]) and os.path.isfile(day1_flIn[-1]):
        data1, data2 = Dataset(day1_flIn[0], 'r'), Dataset(day1_flIn[-1], 'r')
        lon = wrf.getvar(data1, 'lon')[0]
        lat = wrf.getvar(data1, 'lat')[:, 0]
        rain_day1_fcst = (np.array(data2['RAINC'][0]) + np.array(data2['RAINNC'][0])) - (np.array(data1['RAINC'][0]) + np.array(data1['RAINNC'][0]))
        data1.close()
        data2.close()
        intpTmp = RectBivariateSpline(lat, lon, rain_day1_fcst, kx=1, ky=1)
        rain_day1_imerg = intpTmp(lat_imerg, lon_imerg)
    else:
      rain_day1_imerg = np.zeros((len(lat_imerg), len(lon_imerg))) * np.nan
    
    if len(day2_flIn) > 0:
      if os.path.isfile(day2_flIn[0]) and os.path.isfile(day2_flIn[-1]):
        data1, data2 = Dataset(day2_flIn[0], 'r'), Dataset(day2_flIn[-1], 'r')
        lon = wrf.getvar(data1, 'lon')[0]
        lat = wrf.getvar(data1, 'lat')[:, 0]
        rain_day2_fcst = (np.array(data2['RAINC'][0]) + np.array(data2['RAINNC'][0])) - (np.array(data1['RAINC'][0]) + np.array(data1['RAINNC'][0]))
        data1.close()
        data2.close()
        intpTmp = RectBivariateSpline(lat, lon, rain_day2_fcst, kx=1, ky=1)
        rain_day2_imerg = intpTmp(lat_imerg, lon_imerg)
    else:
      rain_day2_imerg = np.zeros((len(lat_imerg), len(lon_imerg))) * np.nan


    # # # 4. Save to combined netCDF file.


    outFl = Dataset(saveFl, 'w')

    outFl.createDimension('lon', len(lon_imerg))
    outFl.createDimension('lat', len(lat_imerg))

    v0 = outFl.createVariable('lon', 'f8', ('lon'))
    v0[:] = lon_imerg
    v0.units = 'degrees_east'

    v0 = outFl.createVariable('lat', 'f8', ('lat'))
    v0[:] = lat_imerg
    v0.units = 'degrees_north'

    v1 = outFl.createVariable('OBS', 'f8', ('lat', 'lon'))
    v1[:] = rain_imerg
    v1.units = 'mm/h'

    v1 = outFl.createVariable('MOD_d0', 'f8', ('lat', 'lon'))
    v1[:] = rain_day0_imerg
    v1.units = 'mm/h'

    v1 = outFl.createVariable('MOD_d1', 'f8', ('lat', 'lon'))
    v1[:] = rain_day1_imerg
    v1.units = 'mm/h'


    v1 = outFl.createVariable('MOD_d2', 'f8', ('lat', 'lon'))
    v1[:] = rain_day2_imerg
    v1.units = 'mm/h'

    outFl.close()
    print('... Wrote processed fields to ' + saveFl.split('/')[-1] + '.')


    # 5. Remove original observed file to save space.
    for fl in imerg_flIn:
      os.remove(fl)
