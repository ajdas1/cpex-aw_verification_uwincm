"""
Ajda Savarin
University of Washington
Created: 9 August 2021

AIRS download link: https://earthdata.nasa.gov/earth-observation-data/near-real-time/download-nrt-data/airs-nrt

"""

from datetime import datetime, timedelta
from netCDF4 import Dataset
from urllib import request

import numpy as np
import os
import subprocess
import sys

import warnings
warnings.filterwarnings('ignore')


args = sys.argv

if len(args) == 2:
  date = args[1]
  date_dt = datetime.strptime(date, '%Y%m%d%H')
  end_time = date_dt
elif len(args) == 3:
  date = args[1]
  date_dt = datetime.strptime(date, '%Y%m%d%H')
  date = args[2]
  end_time = datetime.strptime(date, '%Y%m%d%H')
else:
  date_dt = datetime.now()
  end_time = date_dt

if date_dt.hour == 0:
  day_start = (date_dt-timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
else:
  day_start = date_dt.replace(hour=0, minute=0, second=0, microsecond=0)
  
  
  
cwd = os.getcwd()
cwd_split = cwd.split('/')
baseDir = ('/').join(cwd_split[:-1])

server = 'https://discnrt1.gesdisc.eosdis.nasa.gov/data/Aqua_AIRS_NRT/AIRS2RET_NRT.7.0/'

saveDir = baseDir + '/data/AIRS-nrt/'

latMin, latMax, lonMin, lonMax = 4, 43, -88, -15

def download_nrtAIRS(date_dt):

  filePath = server + date_dt.strftime('%Y/%j/')

  if os.path.isfile(saveFile):    
    print('... AIRS-nrt date ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' already exists.')

  else:
    valid = 0
    for fl in getFile:
      rtn = subprocess.call(['wget', '-N', '-np', '-nH', '--cut-dirs', '6', '-A', 'hdf', '-r', filePath, '-P', saveDir], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
      if rtn == 0:
        print('    ... Downloaded ' + fl)
      valid += rtn
    
    if valid == 0: 
      print('... AIRS-nrt date ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' downloaded successfully.') 
      
      fls = sorted([fl for fl in os.listdir(saveDir) if 'hdf' in fl])
      
      good_chunks = []
      for fl in fls:
        data = Dataset(saveDir+fl, 'r')
        lat = np.array(data['Latitude'])
        lon = np.array(data['Longitude'])
        data.close()
        yDim, xDim = lat.shape
      
        pts = 0
        for pty in range(yDim):
          for ptx in range(xDim):
            if (latMin <= lat[pty, ptx] <= latMax) and (lonMin <= lon[pty, ptx] <= lonMax):
              pts += 1
        if pts > 0:
          good_chunks.append(fl)
           
      lons = []
      lats = [] 
      for fl in good_chunks:
        data = Dataset(saveDir+fl, 'r')
        lats.append(np.array(data['Latitude']))
        lons.append(np.array(data['Longitude']))
        data.close()              
      

       
      
      rain = []
      data = Dataset(saveDir+fileName[0], 'r')
      lon = np.array(data['Grid/lon'])
      lat = np.array(data['Grid/lat'])
      rain.append(np.swapaxes(np.array(data['Grid/precipitationCal'][0]), 0, 1))
      data.close()
      data = Dataset(saveDir+fileName[1], 'r')
      rain.append(np.swapaxes(np.array(data['Grid/precipitationCal'][0]), 0, 1))
      data.close()      
      rain = np.stack(rain)
      rain[rain < -99] = np.nan
      
      rain = np.nanmean(rain, 0)
      
      data = Dataset(saveFile, 'w')
      
      data.createDimension('lon', len(lon))
      data.createDimension('lat', len(lat))
      
      v0 = data.createVariable('lon', 'f8', ('lon'))
      v0[:] = lon
      v0.units = 'degrees_east'
      
      v0 = data.createVariable('lat', 'f8', ('lat'))
      v0[:] = lat
      v0.units = 'degrees_north'
      
      v1 = data.createVariable('precipitation', 'f8', ('lat', 'lon'))
      v1[:] = rain
      v1.units = 'mm/hr'
      
      data.close()
    
      for fl in fileName:
        os.remove(saveDir+fl)
      
      
    else:
      print('... IMERG-E date ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' is currently not available.')

  return


while date_dt <= end_time:
  download_earlyIMERG(date_dt)
  date_dt += timedelta(hours=1)





