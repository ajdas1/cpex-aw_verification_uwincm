"""
Ajda Savarin
University of Washington
Created: 6 August 2021

Run process_imerg-early_forecast for a range of dates.
"""

from datetime import datetime, timedelta
import os
import subprocess
import sys

if len(sys.argv) < 2:
  startTime = '2021073000'
  endTime = '2021073023'
else:
  startTime = sys.argv[1]
  endTime = sys.argv[2]

cwd = os.getcwd()
cwd_split = cwd.split('/')

baseDir = ('/').join(cwd_split[:-1])


fl_process = baseDir + '/processing_scripts/process_imerg-early_forecast.py'



startTime_dt = datetime.strptime(startTime, '%Y%m%d%H')
endTime_dt = datetime.strptime(endTime, '%Y%m%d%H')

currentTime = startTime_dt
while currentTime <= endTime_dt:
  print(currentTime)
  subprocess.call(['python', fl_process, currentTime.strftime('%Y%m%d%H')])
  
  
  currentTime += timedelta(hours=1)
