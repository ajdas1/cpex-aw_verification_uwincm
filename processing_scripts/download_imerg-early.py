"""
Ajda Savarin
University of Washington
Created: 6 August 2021

NASA GES requires a login. It must be set in a .netrc file for this script to work.
For example:
machine gpm1.gesdisc.eosdis.nasa.gov
login foxmulderFBI
password trustNo1
"""

from datetime import datetime, timedelta
from netCDF4 import Dataset

import numpy as np
import os
import subprocess
import sys

import warnings
warnings.filterwarnings('ignore')


args = sys.argv

if len(args) == 2:
  date = args[1]
  date_dt = datetime.strptime(date, '%Y%m%d%H')
  end_time = date_dt
elif len(args) == 3:
  date = args[1]
  date_dt = datetime.strptime(date, '%Y%m%d%H')
  date = args[2]
  end_time = datetime.strptime(date, '%Y%m%d%H')
else:
  date_dt = datetime.now()
  end_time = date_dt

if date_dt.hour == 0:
  day_start = (date_dt-timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
else:
  day_start = date_dt.replace(hour=0, minute=0, second=0, microsecond=0)
  
  
  
cwd = os.getcwd()
cwd_split = cwd.split('/')
baseDir = ('/').join(cwd_split[:-1])

server = 'https://gpm1.gesdisc.eosdis.nasa.gov/data/GPM_L3/GPM_3IMERGHHE.06/'

saveDir = baseDir + '/data/IMERG-early/'



def download_earlyIMERG(date_dt):
  min_since_day_start = [int((date_dt - timedelta(minutes=60) - day_start).total_seconds()/60)%1440, int((date_dt - timedelta(minutes=30) - day_start).total_seconds()/60)%1440]
  
  
  if date_dt.hour == 0:
    filePath = server + (date_dt-timedelta(days=1)).strftime('%Y/%j/')
    fileName = ['3B-HHR-E.MS.MRG.3IMERG.' + (date_dt-timedelta(days=1)).strftime('%Y%m%d-S') + (date_dt-timedelta(minutes=60)).strftime('%H%M%S-E') + (date_dt-timedelta(minutes=30, seconds=1)).strftime('%H%M%S') + '.' + '{:04d}'.format(min_since_day_start[0]) + '.V06B.HDF5', '3B-HHR-E.MS.MRG.3IMERG.' + (date_dt-timedelta(days=1)).strftime('%Y%m%d-S') + (date_dt-timedelta(minutes=30)).strftime('%H%M%S-E') + (date_dt-timedelta(minutes=0, seconds=1)).strftime('%H%M%S') + '.' + '{:04d}'.format(min_since_day_start[1]) + '.V06B.HDF5']  
  else:
    filePath = server + date_dt.strftime('%Y/%j/')
    fileName = ['3B-HHR-E.MS.MRG.3IMERG.' + date_dt.strftime('%Y%m%d-S') + (date_dt-timedelta(minutes=60)).strftime('%H%M%S-E') + (date_dt-timedelta(minutes=30, seconds=1)).strftime('%H%M%S') + '.' + '{:04d}'.format(min_since_day_start[0]) + '.V06B.HDF5', '3B-HHR-E.MS.MRG.3IMERG.' + date_dt.strftime('%Y%m%d-S') + (date_dt-timedelta(minutes=30)).strftime('%H%M%S-E') + (date_dt-timedelta(minutes=0, seconds=1)).strftime('%H%M%S') + '.' + '{:04d}'.format(min_since_day_start[1]) + '.V06B.HDF5']
  
  getFile = [filePath + fn for fn in fileName]
  saveFile = saveDir + 'IMERG-E_' + date_dt.strftime('%Y%m%d%H') + '.nc'

  if os.path.isfile(saveFile):    
    print('... IMERG-E date ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' already exists.')

  else:
    valid = 0
    for fl in getFile:
      rtn = subprocess.call(['wget', '-N', '-np', '-nH', '--cut-dirs', '6', '-A', 'HDF5', '-r', fl, '-P', saveDir], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
      if rtn == 0:
        print('    ... Downloaded ' + fl)
      valid += rtn
    
    if valid == 0: 
      print('... IMERG-E date ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' downloaded successfully.')   
      
      rain = []
      data = Dataset(saveDir+fileName[0], 'r')
      lon = np.array(data['Grid/lon'])
      lat = np.array(data['Grid/lat'])
      rain.append(np.swapaxes(np.array(data['Grid/precipitationCal'][0]), 0, 1))
      data.close()
      data = Dataset(saveDir+fileName[1], 'r')
      rain.append(np.swapaxes(np.array(data['Grid/precipitationCal'][0]), 0, 1))
      data.close()      
      rain = np.stack(rain)
      rain[rain < -99] = np.nan
      
      rain = np.nanmean(rain, 0)
      
      data = Dataset(saveFile, 'w')
      
      data.createDimension('lon', len(lon))
      data.createDimension('lat', len(lat))
      
      v0 = data.createVariable('lon', 'f8', ('lon'))
      v0[:] = lon
      v0.units = 'degrees_east'
      
      v0 = data.createVariable('lat', 'f8', ('lat'))
      v0[:] = lat
      v0.units = 'degrees_north'
      
      v1 = data.createVariable('precipitation', 'f8', ('lat', 'lon'))
      v1[:] = rain
      v1.units = 'mm/hr'
      
      data.close()
    
      for fl in fileName:
        os.remove(saveDir+fl)
      
      
    else:
      print('... IMERG-E date ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' is currently not available.')

  return


while date_dt <= end_time:
  download_earlyIMERG(date_dt)
  date_dt += timedelta(hours=1)





