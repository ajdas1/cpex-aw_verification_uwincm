from matplotlib.gridspec import GridSpec
from matplotlib.colors import LinearSegmentedColormap, ListedColormap

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np

def plot_fig(cmap):
  fig = plt.figure(figsize = (10, 7))
  ax = fig.add_subplot(111)
  f1 = ax.contourf(xx, yy, nums, levels=levs, cmap=cmap, extend='both')
  plt.colorbar(f1, ticks=np.linspace(0, 1, 11), label=cmap, orientation='horizontal', pad=0.05)
  plt.show()
  return
 


# # # COLOR MAP: MIMIC-TPW
def mimic_tpw():
  cmapList = ['#B06E46', '#AC664A', '#A85E4F', '#A45654', '#A04E58', '#9C475C', '#983F61', '#953865', '#91306A', '#8D296E', '#892272', '#851B77', '#82157B', '#7E0E80', '#7A0784', '#6C1592', '#5E22A1', '#4F30AF', '#413DBE', '#334BCC', '#2D59D6', '#2667E0', '#2076EB', '#1984F5', '#1392FF', '#21A0FF', '#2EADFF', '#3CBBFF', '#49C8FF', '#57D6FF', '#5FDEF1', '#67E6E3', '#70EFD5', '#78F7C7', '#80FFB9', '#8EFFAE', '#9CFFA2', '#A9FF97', '#B7FF8B', '#C5FF80', '#D0F677', '#DCEC6D', '#E7E364', '#F3D95A', '#FED051', '#FEC344', '#FEB637', '#FFA829', '#FF9B1C', '#FF8E0F', '#F37F0D', '#E7700B', '#DB6108', '#CF5206', '#C34304', '#B73704', '#AB2B04', '#A01E03', '#941203', '#880603']
  newCmp = ListedColormap(cmapList, name='mimic_tpw', N=None)
  newCmp.set_over('#790609')
  newCmp.set_under('#B77C5B')
  cm.register_cmap(name=newCmp.name, cmap=newCmp)
  return newCmp 

def mimic_tpw_r():
  newCmp = mimic_tpw().reversed()
  cm.register_cmap(name='mimic_tpw_r', cmap=newCmp)
  return newCmp
















if __name__ == "__main__":
  cmap_list = [ncview_jaisnb(), ncview_jaisnc(), ncview_jaisnd(), ncview_3gauss(), ncview_ssec(), ncview_bright(), ncview_banded(), ncview_rainbow(), ncview_detail(),  ncview_manga(), ncview_helix(), ncview_helix2(), ncview_hotres(), mimic_tpw(), uwnd(), wbgyr(), rainfall(), olr_contrast()]

  x = np.arange(-np.pi, np.pi, 0.02)
  y = np.arange(0, 2*np.pi, 0.02)
  xx, yy = np.meshgrid(x, y)
  nums = np.cos(0.5*xx) * np.sin(yy)
  nums = (nums - nums.min()) * 0.5
  levs = np.linspace(0, 1, 256)
  
  cbar_handles = []
  for cmap in cmap_list:
    print(cmap.name)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cbar_handles.append(ax.contourf(xx, yy, nums, levels=levs, cmap=cmap.name))
    plt.close('all')


  fig = plt.figure(figsize = (10, 10))
  gs = GridSpec(len(cmap_list), 1)
  gs.update(hspace=1.5)
  for num, cm in enumerate(cmap_list):
    ax = fig.add_subplot(gs[num, 0])
    ax.tick_params(labeltop=False, labelbottom=False, labelleft=False, labelright=False)
    ax.set_title(cm.name, fontweight='semibold')
    plt.colorbar(cbar_handles[num], ticks=[], orientation='horizontal', cax=ax)
  plt.savefig('colormaps_module.png', dpi=200, bbox_inches='tight')
  plt.close('all')







   
  
  
  
