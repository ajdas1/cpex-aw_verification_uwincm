"""
Ajda Savarin
University of Washington
Created: August 2, 2021

1. Download the latest available data.
2. Process the latest available data.
3. Plot the latest available data in 4 ways:
	- single snapshot (at latest time)
	- latest 24-hrs
	- latest 48-hrs
	- latest 72-hrs
	- since July 26 (beginning of dry run)
"""

import matplotlib as mpl
mpl.use('Agg')

import os
#os.environ['PROJ_LIB'] = '/home/disk/p/asavarin/anaconda3/share/proj'

from datetime import datetime, timedelta
from matplotlib.gridspec import GridSpec
from mpl_toolkits.basemap import Basemap
from netCDF4 import Dataset
from scipy.interpolate import RectBivariateSpline
from urllib import request, error


import colormaps as cms
import matplotlib.pyplot as plt
import numpy as np

import wrf

import warnings
warnings.filterwarnings('ignore')


date_dt = datetime.now() + timedelta(hours=7) # get it into UTC from Pacific
date = date_dt.strftime('%Y%m%d%H')

step1 = True # download MIMIC data
step2 = True # read in model data and get on MIMIC grid, create model comparison files
step3 = True # plot
step3_1 = True # plot latest hour
step3_2 = True # plot latest 24 hours
step3_3 = True # plot latest 48 hours
step3_4 = True # plot latest 72 hours
step3_5 = True # plot record


record_start = datetime.strptime('2021072600', '%Y%m%d%H') 
# UWIN-CM started regularly running on Jul 26, 2021


cwd = os.getcwd()
cwd_split = cwd.split('/')
baseDir = ('/').join(cwd_split[:-1])

mimicDir = baseDir + '/data/MIMIC-TPW/'
fcstDir = '/home/orca3/bkerns/models/uwincm-cpex-aw/'
dataDir = baseDir + '/data/MIMIC-model/'
saveDir = baseDir + '/figs/'

flName = 'MIMIC-model_' + date_dt.strftime('%Y%m%d%H') + '.nc'

# if model comparison file already exists, don't re-download data and re-process it
if os.path.isfile(dataDir+flName):
  step1 = False
  step2 = False
  step3 = False
  print('Current time is already plotted. Will not update images.')

# # # 1. Download the latest available MIMIC-TPW data.
if step1:
  server = 'ftp://ftp.ssec.wisc.edu/pub/mtpw2/data/'


  def download_mimicTPW(date_dt):
    filePath = server + date_dt.strftime('%Y%m/')
    fileName = 'comp' + date_dt.strftime('%Y%m%d.%H') + '0000.nc'
  
    getFile = filePath + fileName
    saveFile = mimicDir+'MIMIC-TPW_'+date_dt.strftime('%Y%m%d%H')+'.nc'
    
    step2 = True
    step3 = True

    if os.path.isfile(saveFile):    
      print(' ... MIMIC-TPW data ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' already exists.')
      rtn = 1
      step2 = False
      step3 = False
    else:
      try:
        request.urlretrieve(getFile, saveFile)    
        print('... MIMIC-TPW data ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' downloaded successfully.')
        rtn = 1
      except error.HTTPError:
        print('... MIMIC-TPW data ' + date_dt.strftime('%Y-%m-%d %H UTC') + ' is currently not available.')
        rtn = 0
        step2 = False
        step3 = False

    return rtn, step2, step3

  rtn = 0
  while rtn == 0:
    print(date_dt.strftime('%Y-%m-%d %H UTC'))
    try:
      rtn = download_mimicTPW(date_dt)
      
    except error.URLError:
      print(' ... File does not exist')
      rtn = 0
      date_dt -= timedelta(hours=1)

# # # 2. Process the latest available MIMIC-TPW data and contemporary model data.
if step2:
  day0_fcstDir = fcstDir + date_dt.strftime('%Y%m%d' + '00/')
  day1_fcstDir = fcstDir + (date_dt-timedelta(days=1)).strftime('%Y%m%d' + '00/')
  day2_fcstDir = fcstDir + (date_dt-timedelta(days=2)).strftime('%Y%m%d' + '00/')
  day0_flIn = day0_fcstDir + 'wrfout_d01_' + date_dt.strftime('%Y-%m-%d_%H:00:00')
  day1_flIn = day1_fcstDir + 'wrfout_d01_' + date_dt.strftime('%Y-%m-%d_%H:00:00')
  day2_flIn = day2_fcstDir + 'wrfout_d01_' + date_dt.strftime('%Y-%m-%d_%H:00:00')
  if os.path.isfile(dataDir+flName):
    print(' ... Processed file already exists.')
  else:
    print(' ... Processing file.')
  
    mimic_flIn = mimicDir + 'MIMIC-TPW_' + date_dt.strftime('%Y%m%d%H') + '.nc'

    data = Dataset(mimic_flIn, 'r')
    lon_mimic = np.array(data['lonArr'])[374:659]
    lat_mimic = np.array(data['latArr'])[380:529]
    tpw_mimic = np.array(data['tpwGrid'])[380:529, 374:659]
    data.close()

    data = Dataset(day0_flIn, 'r')
    lon = wrf.getvar(data, 'lon')[0]
    lat = wrf.getvar(data, 'lat')[:, 0]
    tpw_day0_fcst = wrf.getvar(data, 'pw')
    data.close()

    data = Dataset(day1_flIn, 'r')
    tpw_day1_fcst = wrf.getvar(data, 'pw')
    data.close()

    data = Dataset(day2_flIn, 'r')
    tpw_day2_fcst = wrf.getvar(data, 'pw')
    data.close()

    intpTmp = RectBivariateSpline(lat, lon, tpw_day0_fcst)
    tpw_day0_mimic = intpTmp(lat_mimic, lon_mimic)
    intpTmp = RectBivariateSpline(lat, lon, tpw_day1_fcst)
    tpw_day1_mimic = intpTmp(lat_mimic, lon_mimic)
    intpTmp = RectBivariateSpline(lat, lon, tpw_day2_fcst)
    tpw_day2_mimic = intpTmp(lat_mimic, lon_mimic)


    outFl = Dataset(dataDir + flName, 'w')

    outFl.createDimension('lon', len(lon_mimic))
    outFl.createDimension('lat', len(lat_mimic))

    v0 = outFl.createVariable('lon', 'f8', ('lon'))
    v0[:] = lon_mimic
    v0.units = 'degrees_east'

    v0 = outFl.createVariable('lat', 'f8', ('lat'))
    v0[:] = lat_mimic
    v0.units = 'degrees_north'

    v1 = outFl.createVariable('OBS', 'f8', ('lat', 'lon'))
    v1[:] = tpw_mimic
    v1.units = 'mm'

    v1 = outFl.createVariable('MOD_d0', 'f8', ('lat', 'lon'))
    v1[:] = tpw_day0_mimic
    v1.units = 'mm'

    v1 = outFl.createVariable('MOD_d1', 'f8', ('lat', 'lon'))
    v1[:] = tpw_day1_mimic
    v1.units = 'mm'


    v1 = outFl.createVariable('MOD_d2', 'f8', ('lat', 'lon'))
    v1[:] = tpw_day2_mimic
    v1.units = 'mm'

    data.close()


    os.remove(mimic_flIn)
    
    del lon_mimic, lat_mimic, tpw_mimic, lon, lat, tpw_day0_fcst, tpw_day1_fcst, tpw_day2_fcst

# # # Plot the latest available data in 5 time frames:
if step3:
  m = Basemap(llcrnrlon=-86.5, llcrnrlat=5, urcrnrlon=-15.5, urcrnrlat=42, resolution='i')

  def plot_mapFramework(m, ax, latLabel=[1,0,0,0]):
    m.drawcoastlines(ax=ax)
    m.drawparallels(np.arange(10, 45, 10), labels=latLabel, linewidth=.8, ax=ax)
    m.drawparallels(np.arange(5, 45, 10), labels=[0,0,0,0], linewidth=.5, ax=ax)
    m.drawmeridians(np.arange(-80, -10, 10), labels=[0,0,0,1], linewidth=.8, ax=ax)
    m.drawmeridians(np.arange(-85, -10, 10), labels=[0,0,0,0], linewidth=.5, ax=ax)
  
    return
    
  gsLayout = True
  mapFramework = True
  plotShading = True
  setLabels = True
  

  if step3_1:
    print(' ... Plotting latest hour summary figure.')
    data = Dataset(dataDir + flName, 'r')
    lon = np.array(data['lon'])
    lat = np.array(data['lat'])
    tpw_obs = np.array(data['OBS'])
    tpw_dy0 = np.array(data['MOD_d0'])
    tpw_dy1 = np.array(data['MOD_d1'])
    tpw_dy2 = np.array(data['MOD_d2'])
    data.close()
    lonGrid, latGrid = np.meshgrid(lon, lat)
  
    tpw_obs = np.ma.masked_invalid(tpw_obs)
    r2_dy0 = (np.corrcoef(np.ravel(tpw_obs[tpw_obs.mask==False]), np.ravel(tpw_dy0[tpw_obs.mask==False]))[0, 1])**2
    r2_dy1 = (np.corrcoef(np.ravel(tpw_obs[tpw_obs.mask==False]), np.ravel(tpw_dy1[tpw_obs.mask==False]))[0, 1])**2
    r2_dy2 = (np.corrcoef(np.ravel(tpw_obs[tpw_obs.mask==False]), np.ravel(tpw_dy2[tpw_obs.mask==False]))[0, 1])**2


    fig = plt.figure(figsize = (12, 12))

    if gsLayout:
      gs = GridSpec(4, 4, width_ratios =[.08, 1, 1, .08])

      ax_obs = fig.add_subplot(gs[0, 1])
      ax_md0 = fig.add_subplot(gs[1, 1])
      ax_md1 = fig.add_subplot(gs[2, 1])
      ax_md2 = fig.add_subplot(gs[3, 1])

      ax_dd0 = fig.add_subplot(gs[1, 2])
      ax_dd1 = fig.add_subplot(gs[2, 2])
      ax_dd2 = fig.add_subplot(gs[3, 2])

      cax_mm = fig.add_subplot(gs[:, 0])
      cax_df = fig.add_subplot(gs[1:, 3])
    
      ax_obs.set_facecolor('.3')
      ax_md0.set_facecolor('.3')
      ax_md1.set_facecolor('.3')
      ax_md2.set_facecolor('.3')
      ax_dd0.set_facecolor('.3')
      ax_dd1.set_facecolor('.3')
      ax_dd2.set_facecolor('.3')
  
    if mapFramework:
      plot_mapFramework(m, ax_obs, latLabel=[0,1,0,0])
      plot_mapFramework(m, ax_md0, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md1, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md2, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_dd0)
      plot_mapFramework(m, ax_dd1)
      plot_mapFramework(m, ax_dd2)

    if plotShading:
      f1 = m.contourf(lonGrid, latGrid, tpw_obs, levels=np.arange(5, 66, 1), cmap=cms.mimic_tpw(), ax=ax_obs, extend='both')
      m.contourf(lonGrid, latGrid, tpw_dy0, levels=np.arange(5, 66, 1), cmap=cms.mimic_tpw(), ax=ax_md0, extend='both')
      m.contourf(lonGrid, latGrid, tpw_dy1, levels=np.arange(5, 66, 1), cmap=cms.mimic_tpw(), ax=ax_md1, extend='both')
      m.contourf(lonGrid, latGrid, tpw_dy2, levels=np.arange(5, 66, 1), cmap=cms.mimic_tpw(), ax=ax_md2, extend='both')
  
      f2 = m.contourf(lonGrid, latGrid, tpw_dy0-tpw_obs, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_dd0)
      m.contourf(lonGrid, latGrid, tpw_dy1-tpw_obs, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_dd1)
      m.contourf(lonGrid, latGrid, tpw_dy2-tpw_obs, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_dd2)
      
      m.contour(lonGrid, latGrid, tpw_obs, levels=[50, 100], colors='k', linewidths=1, ax=ax_obs)
      m.contour(lonGrid, latGrid, tpw_obs, levels=[50, 100], colors='k', linewidths=1, ax=ax_dd0)
      m.contour(lonGrid, latGrid, tpw_obs, levels=[50, 100], colors='k', linewidths=1, ax=ax_dd1)
      m.contour(lonGrid, latGrid, tpw_obs, levels=[50, 100], colors='k', linewidths=1, ax=ax_dd2)      
      m.contour(lonGrid, latGrid, tpw_dy0, levels=[50, 100], colors='k', linewidths=1, ax=ax_md0)    
      m.contour(lonGrid, latGrid, tpw_dy1, levels=[50, 100], colors='k', linewidths=1, ax=ax_md1)    
      m.contour(lonGrid, latGrid, tpw_dy2, levels=[50, 100], colors='k', linewidths=1, ax=ax_md2)
      m.contourf(lonGrid, latGrid, tpw_obs, levels=[50, 100], colors='k', linewidths=1, alpha=.2, ax=ax_dd0)
      m.contourf(lonGrid, latGrid, tpw_obs, levels=[50, 100], colors='k', linewidths=1, alpha=.2, ax=ax_dd1)
      m.contourf(lonGrid, latGrid, tpw_obs, levels=[50, 100], colors='k', linewidths=1, alpha=.2, ax=ax_dd2)
  
      cbar_mm = plt.colorbar(f1, cax=cax_mm, ticks=np.arange(5, 66, 5))
      cbar_df = plt.colorbar(f2, cax=cax_df, ticks=np.arange(-10, 11, 2))

    if setLabels:
      ax_obs.set_title('TPW ' + date_dt.strftime('%Y-%m-%d %H UTC') + '\n MIMIC-TPW', fontweight='semibold')
      ax_md0.set_title('UWIN-CM init: ' + date_dt.strftime('%Y-%m-%d 00 UTC'), fontweight='semibold')
      ax_md1.set_title('UWIN-CM init: ' + (date_dt-timedelta(days=1)).strftime('%Y-%m-%d 00 UTC'), fontweight='semibold')
      ax_md2.set_title('UWIN-CM init: ' + (date_dt-timedelta(days=2)).strftime('%Y-%m-%d 00 UTC'), fontweight='semibold')
  
      ax_dd0.set_title('UWIN-CM  -  MIMIC-TPW \n Day 0 forecast; $r^2$=' + '{:.2f}'.format(r2_dy0), fontweight='semibold')
      ax_dd1.set_title('Day 1 forecast; $r^2$=' + '{:.2f}'.format(r2_dy1), fontweight='semibold')
      ax_dd2.set_title('Day 2 forecast; $r^2$=' + '{:.2f}'.format(r2_dy2), fontweight='semibold')
  
      cax_mm.set_title('TPW\n[mm]', fontweight='semibold')
      cax_df.set_title(r'$\Delta$TPW' + '\n[mm]', fontweight='semibold')
      cax_df.yaxis.set_ticklabels([str(int(num)) for num in np.arange(-10, 11, 2)])
      cax_df.yaxis.set_ticks_position('left')

    plt.savefig(saveDir + 'TPW_UWINCM_MIMIC_latest_01h.png', dpi=200, bbox_inches='tight')
    plt.close()

  if step3_2:
    print(' ... Plotting latest 24-hours summary figure.')
    fls = sorted([fl for fl in os.listdir(dataDir) if '.nc' in fl])
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    fls = [fl for num, fl in enumerate(fls) if (date_dt-timedelta(days=1)) < fl_dates[num] <= date_dt]
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    
    data = Dataset(dataDir + fls[0], 'r')
    lon = np.array(data['lon'])
    lat = np.array(data['lat'])
    data.close()
    lonGrid, latGrid = np.meshgrid(lon, lat)
    
    dtpw_d0 = []
    dtpw_d1 = []
    dtpw_d2 = []
    for fl in fls:
      data = Dataset(dataDir + fl, 'r')
      obs = np.array(data['OBS'])
      dtpw_d0.append(np.array(data['MOD_d0']) - obs)
      dtpw_d1.append(np.array(data['MOD_d1']) - obs)
      dtpw_d2.append(np.array(data['MOD_d2']) - obs)
      data.close()

    dtpw_d0 = np.ma.masked_invalid(np.stack(dtpw_d0))
    dtpw_d1 = np.ma.masked_invalid(np.stack(dtpw_d1))
    dtpw_d2 = np.ma.masked_invalid(np.stack(dtpw_d2))
    
    dtpw_d0_mean = np.nanmean(dtpw_d0, 0)
    dtpw_d1_mean = np.nanmean(dtpw_d1, 0)
    dtpw_d2_mean = np.nanmean(dtpw_d2, 0)

    dtpw_d0_std = np.nanstd(dtpw_d0, 0)
    dtpw_d1_std = np.nanstd(dtpw_d1, 0)
    dtpw_d2_std = np.nanstd(dtpw_d2, 0)


    fig = plt.figure(figsize = (12, 9.5))

    if gsLayout:
      gs = GridSpec(3, 4, width_ratios =[.08, 1, 1, .08])

      ax_md0 = fig.add_subplot(gs[0, 1])
      ax_md1 = fig.add_subplot(gs[1, 1])
      ax_md2 = fig.add_subplot(gs[2, 1])

      ax_sd0 = fig.add_subplot(gs[0, 2])
      ax_sd1 = fig.add_subplot(gs[1, 2])
      ax_sd2 = fig.add_subplot(gs[2, 2])

      cax_md = fig.add_subplot(gs[:, 0])
      cax_sd = fig.add_subplot(gs[:, 3])
  
    if mapFramework:
      plot_mapFramework(m, ax_md0, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md1, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md2, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_sd0)
      plot_mapFramework(m, ax_sd1)
      plot_mapFramework(m, ax_sd2)

    if plotShading:
      f1 = m.contourf(lonGrid, latGrid, dtpw_d0_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md0)
      m.contourf(lonGrid, latGrid, dtpw_d1_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md1)
      m.contourf(lonGrid, latGrid, dtpw_d2_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md2)
  
      f2 = m.contourf(lonGrid, latGrid, dtpw_d0_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd0, alpha=.8)
      m.contourf(lonGrid, latGrid, dtpw_d1_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd1, alpha=.8)
      m.contourf(lonGrid, latGrid, dtpw_d2_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd2, alpha=.8)
  
  
      cbar_md = plt.colorbar(f1, cax=cax_md, ticks=np.arange(-10, 11, 2.5))
      cbar_sd = plt.colorbar(f2, cax=cax_sd, ticks=np.arange(1, 10.1, 1))

    if setLabels:
      ax_md0.set_title(fl_dates[0].strftime('(%Y-%m-%d %H - ') + fl_dates[-1].strftime(' %Y-%m-%d %H)') + '\nUWIN-CM day 0 forecast - mean bias', fontweight='semibold')
      ax_md1.set_title('UWIN-CM day 1 forecast - mean bias', fontweight='semibold')
      ax_md2.set_title('UWIN-CM day 2 forecast - mean bias', fontweight='semibold')
  

      ax_sd0.set_title('UWIN-CM day 0 forecast - std', fontweight='semibold')
      ax_sd1.set_title('UWIN-CM day 1 forecast - std', fontweight='semibold')
      ax_sd2.set_title('UWIN-CM day 2 forecast - std', fontweight='semibold')
  
      cax_md.set_title('$\Delta$TPW\n[mm]', fontweight='semibold')
      cax_sd.set_title(r'$\sigma$TPW' + '\n[mm]', fontweight='semibold')
      cax_md.yaxis.set_ticklabels([str(int(num)) for num in np.arange(-10, 11, 2.5)])
      cax_sd.yaxis.set_ticks_position('left')

    plt.savefig(saveDir + 'TPW_UWINCM_MIMIC_latest_24h.png', dpi=200, bbox_inches='tight')
    plt.close()

  if step3_3:
    print(' ... Plotting latest 48-hours summary figure.')
    fls = sorted([fl for fl in os.listdir(dataDir) if '.nc' in fl])
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    fls = [fl for num, fl in enumerate(fls) if (date_dt-timedelta(days=2)) < fl_dates[num] <= date_dt]
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    
    data = Dataset(dataDir + fls[0], 'r')
    lon = np.array(data['lon'])
    lat = np.array(data['lat'])
    data.close()
    lonGrid, latGrid = np.meshgrid(lon, lat)
    
    dtpw_d0 = []
    dtpw_d1 = []
    dtpw_d2 = []
    for fl in fls:
      data = Dataset(dataDir + fl, 'r')
      obs = np.array(data['OBS'])
      dtpw_d0.append(np.array(data['MOD_d0']) - obs)
      dtpw_d1.append(np.array(data['MOD_d1']) - obs)
      dtpw_d2.append(np.array(data['MOD_d2']) - obs)
      data.close()

    dtpw_d0 = np.ma.masked_invalid(np.stack(dtpw_d0))
    dtpw_d1 = np.ma.masked_invalid(np.stack(dtpw_d1))
    dtpw_d2 = np.ma.masked_invalid(np.stack(dtpw_d2))
    
    dtpw_d0_mean = np.nanmean(dtpw_d0, 0)
    dtpw_d1_mean = np.nanmean(dtpw_d1, 0)
    dtpw_d2_mean = np.nanmean(dtpw_d2, 0)

    dtpw_d0_std = np.nanstd(dtpw_d0, 0)
    dtpw_d1_std = np.nanstd(dtpw_d1, 0)
    dtpw_d2_std = np.nanstd(dtpw_d2, 0)


    fig = plt.figure(figsize = (12, 9.5))

    if gsLayout:
      gs = GridSpec(3, 4, width_ratios =[.08, 1, 1, .08])

      ax_md0 = fig.add_subplot(gs[0, 1])
      ax_md1 = fig.add_subplot(gs[1, 1])
      ax_md2 = fig.add_subplot(gs[2, 1])

      ax_sd0 = fig.add_subplot(gs[0, 2])
      ax_sd1 = fig.add_subplot(gs[1, 2])
      ax_sd2 = fig.add_subplot(gs[2, 2])

      cax_md = fig.add_subplot(gs[:, 0])
      cax_sd = fig.add_subplot(gs[:, 3])
  
    if mapFramework:
      plot_mapFramework(m, ax_md0, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md1, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md2, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_sd0)
      plot_mapFramework(m, ax_sd1)
      plot_mapFramework(m, ax_sd2)

    if plotShading:
      f1 = m.contourf(lonGrid, latGrid, dtpw_d0_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md0)
      m.contourf(lonGrid, latGrid, dtpw_d1_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md1)
      m.contourf(lonGrid, latGrid, dtpw_d2_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md2)
  
      f2 = m.contourf(lonGrid, latGrid, dtpw_d0_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd0, alpha=.8)
      m.contourf(lonGrid, latGrid, dtpw_d1_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd1, alpha=.8)
      m.contourf(lonGrid, latGrid, dtpw_d2_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd2, alpha=.8)
  
  
      cbar_md = plt.colorbar(f1, cax=cax_md, ticks=np.arange(-10, 11, 2.5))
      cbar_sd = plt.colorbar(f2, cax=cax_sd, ticks=np.arange(1, 10.1, 1))

    if setLabels:
      ax_md0.set_title(fl_dates[0].strftime('(%Y-%m-%d %H - ') + fl_dates[-1].strftime(' %Y-%m-%d %H)') + '\nUWIN-CM day 0 forecast - mean bias', fontweight='semibold')
      ax_md1.set_title('UWIN-CM day 1 forecast - mean bias', fontweight='semibold')
      ax_md2.set_title('UWIN-CM day 2 forecast - mean bias', fontweight='semibold')
  

      ax_sd0.set_title('UWIN-CM day 0 forecast - std', fontweight='semibold')
      ax_sd1.set_title('UWIN-CM day 1 forecast - std', fontweight='semibold')
      ax_sd2.set_title('UWIN-CM day 2 forecast - std', fontweight='semibold')
  
      cax_md.set_title('$\Delta$TPW\n[mm]', fontweight='semibold')
      cax_sd.set_title(r'$\sigma$TPW' + '\n[mm]', fontweight='semibold')
      cax_md.yaxis.set_ticklabels([str(int(num)) for num in np.arange(-10, 11, 2.5)])
      cax_sd.yaxis.set_ticks_position('left')

    plt.savefig(saveDir + 'TPW_UWINCM_MIMIC_latest_48h.png', dpi=200, bbox_inches='tight')
    plt.close()

  if step3_4:
    print(' ... Plotting latest 72-hours summary figure.')
    fls = sorted([fl for fl in os.listdir(dataDir) if '.nc' in fl])
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    fls = [fl for num, fl in enumerate(fls) if (date_dt-timedelta(days=3)) < fl_dates[num] <= date_dt]
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    
    data = Dataset(dataDir + fls[0], 'r')
    lon = np.array(data['lon'])
    lat = np.array(data['lat'])
    data.close()
    lonGrid, latGrid = np.meshgrid(lon, lat)
    
    dtpw_d0 = []
    dtpw_d1 = []
    dtpw_d2 = []
    for fl in fls:
      data = Dataset(dataDir + fl, 'r')
      obs = np.array(data['OBS'])
      dtpw_d0.append(np.array(data['MOD_d0']) - obs)
      dtpw_d1.append(np.array(data['MOD_d1']) - obs)
      dtpw_d2.append(np.array(data['MOD_d2']) - obs)
      data.close()

    dtpw_d0 = np.ma.masked_invalid(np.stack(dtpw_d0))
    dtpw_d1 = np.ma.masked_invalid(np.stack(dtpw_d1))
    dtpw_d2 = np.ma.masked_invalid(np.stack(dtpw_d2))
    
    dtpw_d0_mean = np.nanmean(dtpw_d0, 0)
    dtpw_d1_mean = np.nanmean(dtpw_d1, 0)
    dtpw_d2_mean = np.nanmean(dtpw_d2, 0)

    dtpw_d0_std = np.nanstd(dtpw_d0, 0)
    dtpw_d1_std = np.nanstd(dtpw_d1, 0)
    dtpw_d2_std = np.nanstd(dtpw_d2, 0)


    fig = plt.figure(figsize = (12, 9.5))

    if gsLayout:
      gs = GridSpec(3, 4, width_ratios =[.08, 1, 1, .08])

      ax_md0 = fig.add_subplot(gs[0, 1])
      ax_md1 = fig.add_subplot(gs[1, 1])
      ax_md2 = fig.add_subplot(gs[2, 1])

      ax_sd0 = fig.add_subplot(gs[0, 2])
      ax_sd1 = fig.add_subplot(gs[1, 2])
      ax_sd2 = fig.add_subplot(gs[2, 2])

      cax_md = fig.add_subplot(gs[:, 0])
      cax_sd = fig.add_subplot(gs[:, 3])
  
    if mapFramework:
      plot_mapFramework(m, ax_md0, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md1, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md2, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_sd0)
      plot_mapFramework(m, ax_sd1)
      plot_mapFramework(m, ax_sd2)

    if plotShading:
      f1 = m.contourf(lonGrid, latGrid, dtpw_d0_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md0)
      m.contourf(lonGrid, latGrid, dtpw_d1_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md1)
      m.contourf(lonGrid, latGrid, dtpw_d2_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md2)
  
      f2 = m.contourf(lonGrid, latGrid, dtpw_d0_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd0, alpha=.8)
      m.contourf(lonGrid, latGrid, dtpw_d1_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd1, alpha=.8)
      m.contourf(lonGrid, latGrid, dtpw_d2_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd2, alpha=.8)
  
  
      cbar_md = plt.colorbar(f1, cax=cax_md, ticks=np.arange(-10, 11, 2.5))
      cbar_sd = plt.colorbar(f2, cax=cax_sd, ticks=np.arange(1, 10.1, 1))

    if setLabels:
      ax_md0.set_title(fl_dates[0].strftime('(%Y-%m-%d %H - ') + fl_dates[-1].strftime(' %Y-%m-%d %H)') + '\nUWIN-CM day 0 forecast - mean bias', fontweight='semibold')
      ax_md1.set_title('UWIN-CM day 1 forecast - mean bias', fontweight='semibold')
      ax_md2.set_title('UWIN-CM day 2 forecast - mean bias', fontweight='semibold')
  

      ax_sd0.set_title('UWIN-CM day 0 forecast - std', fontweight='semibold')
      ax_sd1.set_title('UWIN-CM day 1 forecast - std', fontweight='semibold')
      ax_sd2.set_title('UWIN-CM day 2 forecast - std', fontweight='semibold')
  
      cax_md.set_title('$\Delta$TPW\n[mm]', fontweight='semibold')
      cax_sd.set_title(r'$\sigma$TPW' + '\n[mm]', fontweight='semibold')
      cax_md.yaxis.set_ticklabels([str(int(num)) for num in np.arange(-10, 11, 2.5)])
      cax_sd.yaxis.set_ticks_position('left')

    plt.savefig(saveDir + 'TPW_UWINCM_MIMIC_latest_72h.png', dpi=200, bbox_inches='tight')
    plt.close()

  if step3_5:
    print(' ... Plotting entire time range summary figure.')
    fls = sorted([fl for fl in os.listdir(dataDir) if '.nc' in fl])
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    fls = [fl for num, fl in enumerate(fls) if record_start <= fl_dates[num] <= date_dt]
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    
    data = Dataset(dataDir + fls[0], 'r')
    lon = np.array(data['lon'])
    lat = np.array(data['lat'])
    data.close()
    lonGrid, latGrid = np.meshgrid(lon, lat)
    
    dtpw_d0 = []
    dtpw_d1 = []
    dtpw_d2 = []
    for fl in fls:
      data = Dataset(dataDir + fl, 'r')
      obs = np.array(data['OBS'])
      dtpw_d0.append(np.array(data['MOD_d0']) - obs)
      dtpw_d1.append(np.array(data['MOD_d1']) - obs)
      dtpw_d2.append(np.array(data['MOD_d2']) - obs)
      data.close()

    dtpw_d0 = np.ma.masked_invalid(np.stack(dtpw_d0))
    dtpw_d1 = np.ma.masked_invalid(np.stack(dtpw_d1))
    dtpw_d2 = np.ma.masked_invalid(np.stack(dtpw_d2))
    
    dtpw_d0_mean = np.nanmean(dtpw_d0, 0)
    dtpw_d1_mean = np.nanmean(dtpw_d1, 0)
    dtpw_d2_mean = np.nanmean(dtpw_d2, 0)

    dtpw_d0_std = np.nanstd(dtpw_d0, 0)
    dtpw_d1_std = np.nanstd(dtpw_d1, 0)
    dtpw_d2_std = np.nanstd(dtpw_d2, 0)


    fig = plt.figure(figsize = (12, 9.5))

    if gsLayout:
      gs = GridSpec(3, 4, width_ratios =[.08, 1, 1, .08])

      ax_md0 = fig.add_subplot(gs[0, 1])
      ax_md1 = fig.add_subplot(gs[1, 1])
      ax_md2 = fig.add_subplot(gs[2, 1])

      ax_sd0 = fig.add_subplot(gs[0, 2])
      ax_sd1 = fig.add_subplot(gs[1, 2])
      ax_sd2 = fig.add_subplot(gs[2, 2])

      cax_md = fig.add_subplot(gs[:, 0])
      cax_sd = fig.add_subplot(gs[:, 3])
  
    if mapFramework:
      plot_mapFramework(m, ax_md0, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md1, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md2, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_sd0)
      plot_mapFramework(m, ax_sd1)
      plot_mapFramework(m, ax_sd2)

    if plotShading:
      f1 = m.contourf(lonGrid, latGrid, dtpw_d0_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md0)
      m.contourf(lonGrid, latGrid, dtpw_d1_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md1)
      m.contourf(lonGrid, latGrid, dtpw_d2_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md2)
  
      f2 = m.contourf(lonGrid, latGrid, dtpw_d0_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd0, alpha=.8)
      m.contourf(lonGrid, latGrid, dtpw_d1_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd1, alpha=.8)
      m.contourf(lonGrid, latGrid, dtpw_d2_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd2, alpha=.8)
  
  
      cbar_md = plt.colorbar(f1, cax=cax_md, ticks=np.arange(-10, 11, 2.5))
      cbar_sd = plt.colorbar(f2, cax=cax_sd, ticks=np.arange(1, 10.1, 1))

    if setLabels:
      ax_md0.set_title(fl_dates[0].strftime('(%Y-%m-%d %H - ') + fl_dates[-1].strftime(' %Y-%m-%d %H)') + '\nUWIN-CM day 0 forecast - mean bias', fontweight='semibold')
      ax_md1.set_title('UWIN-CM day 1 forecast - mean bias', fontweight='semibold')
      ax_md2.set_title('UWIN-CM day 2 forecast - mean bias', fontweight='semibold')
  

      ax_sd0.set_title('UWIN-CM day 0 forecast - std', fontweight='semibold')
      ax_sd1.set_title('UWIN-CM day 1 forecast - std', fontweight='semibold')
      ax_sd2.set_title('UWIN-CM day 2 forecast - std', fontweight='semibold')
  
      cax_md.set_title('$\Delta$TPW\n[mm]', fontweight='semibold')
      cax_sd.set_title(r'$\sigma$TPW' + '\n[mm]', fontweight='semibold')
      cax_md.yaxis.set_ticklabels([str(int(num)) for num in np.arange(-10, 11, 2.5)])
      cax_sd.yaxis.set_ticks_position('left')

    plt.savefig(saveDir + 'TPW_UWINCM_MIMIC_latest_record.png', dpi=200, bbox_inches='tight')
    plt.close()
