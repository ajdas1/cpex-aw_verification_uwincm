"""
Ajda Savarin
University of Washington
Created: August 6, 2021

1. Download the latest available data.
2. Process the latest available data.
3. Plot the latest available data in 4 ways:
	- single snapshot (at latest time)
	- latest 24-hrs
	- latest 48-hrs
	- latest 72-hrs
	- since July 26 (beginning of dry run)
"""

import matplotlib as mpl
mpl.use('Agg')

import os
#os.environ['PROJ_LIB'] = '/home/disk/p/asavarin/anaconda3/share/proj'

from datetime import datetime, timedelta
from matplotlib.gridspec import GridSpec
from mpl_toolkits.basemap import Basemap
from netCDF4 import Dataset
from scipy.interpolate import RectBivariateSpline
from urllib import request, error


import cmaps as cms
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np
import subprocess
import wrf

import warnings
warnings.filterwarnings('ignore')


date_dt = datetime.now() + timedelta(hours=7) # get it into UTC from Pacific
#date_dt = datetime.strptime('2021-08-06 23', '%Y-%m-%d %H')
date = date_dt.strftime('%Y%m%d%H')

step1 = True # download and process MIMIC data
step2 = True # plot
step2_1 = True # plot latest hour
step2_2 = True # plot latest 24 hours
step2_3 = True # plot latest 48 hours
step2_4 = True # plot latest 72 hours
step2_5 = True # plot record


record_start = datetime.strptime('2021072600', '%Y%m%d%H') 
# UWIN-CM started regularly running on Jul 26, 2021


cwd = os.getcwd()
cwd_split = cwd.split('/')
baseDir = ('/').join(cwd_split[:-1])

processDir = baseDir + '/processing_scripts/'
fl_process = processDir + 'process_imerg-early_forecast.py'
dataDir = baseDir + '/data/IMERG-model/'
saveDir = baseDir + '/figs/'


# # # 1. Download the latest available IMERG-early data.
if step1:
  currentTime = date_dt - timedelta(hours=3) 

  while currentTime > (date_dt-timedelta(hours=24)):
    print(currentTime.strftime('%Y-%m-%d %H'))
    prcs = subprocess.call(['python', fl_process, currentTime.strftime('%Y%m%d%H')])

    flName_tmp = 'IMERG-model_' + currentTime.strftime('%Y%m%d%H') + '.nc'
    if os.path.isfile(dataDir+flName_tmp):
      print('Processed data for comparison. Will plot new images.')
      break
    else:
      currentTime -= timedelta(hours=1)

  date_dt = currentTime
  flName = 'IMERG-model_' + date_dt.strftime('%Y%m%d%H') + '.nc'  

# # # Plot the latest available data in 5 time frames:
if step2:
  m = Basemap(llcrnrlon=-86.5, llcrnrlat=5, urcrnrlon=-15.5, urcrnrlat=42, resolution='i')

  def plot_mapFramework(m, ax, latLabel=[1,0,0,0]):
    m.drawcoastlines(ax=ax)
    m.drawparallels(np.arange(10, 45, 10), labels=latLabel, linewidth=.8, ax=ax)
    m.drawparallels(np.arange(5, 45, 10), labels=[0,0,0,0], linewidth=.5, ax=ax)
    m.drawmeridians(np.arange(-80, -10, 10), labels=[0,0,0,1], linewidth=.8, ax=ax)
    m.drawmeridians(np.arange(-85, -10, 10), labels=[0,0,0,0], linewidth=.5, ax=ax)
  
    return
    
  gsLayout = True
  mapFramework = True
  plotShading = True
  setLabels = True
  

  if step2_1:
    print(' ... Plotting latest hour summary figure.')
    data = Dataset(dataDir + flName, 'r')
    lon = np.array(data['lon'])
    lat = np.array(data['lat'])
    rain_obs = np.array(data['OBS'])
    rain_dy0 = np.array(data['MOD_d0'])
    rain_dy1 = np.array(data['MOD_d1'])
    rain_dy2 = np.array(data['MOD_d2'])
    data.close()
    lonGrid, latGrid = np.meshgrid(lon, lat)
  
    rain_obs = np.ma.masked_invalid(rain_obs)
    r2_dy0 = (np.corrcoef(np.ravel(rain_obs[rain_obs.mask==False]), np.ravel(rain_dy0[rain_obs.mask==False]))[0, 1])**2
    r2_dy1 = (np.corrcoef(np.ravel(rain_obs[rain_obs.mask==False]), np.ravel(rain_dy1[rain_obs.mask==False]))[0, 1])**2
    r2_dy2 = (np.corrcoef(np.ravel(rain_obs[rain_obs.mask==False]), np.ravel(rain_dy2[rain_obs.mask==False]))[0, 1])**2

    fig = plt.figure(figsize = (12, 12))

    if gsLayout:
      gs = GridSpec(4, 4, width_ratios =[.08, 1, 1, .08])

      ax_obs = fig.add_subplot(gs[0, 1])
      ax_md0 = fig.add_subplot(gs[1, 1])
      ax_md1 = fig.add_subplot(gs[2, 1])
      ax_md2 = fig.add_subplot(gs[3, 1])

      ax_dd0 = fig.add_subplot(gs[1, 2])
      ax_dd1 = fig.add_subplot(gs[2, 2])
      ax_dd2 = fig.add_subplot(gs[3, 2])

      cax_mm = fig.add_subplot(gs[:, 0])
      cax_df = fig.add_subplot(gs[1:, 3])
 
    if mapFramework:
      plot_mapFramework(m, ax_obs, latLabel=[0,1,0,0])
      plot_mapFramework(m, ax_md0, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md1, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md2, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_dd0)
      plot_mapFramework(m, ax_dd1)
      plot_mapFramework(m, ax_dd2)

    if plotShading:

      f1 = m.contourf(lonGrid, latGrid, rain_obs, cmap=cms.WhiteBlueGreenYellowRed, levels=np.logspace(np.log10(0.1), np.log10(20.1), 25), extend='max', norm=colors.LogNorm(), vmin=0.02, ax=ax_obs)
      m.contourf(lonGrid, latGrid, rain_dy0, cmap=cms.WhiteBlueGreenYellowRed, levels=np.logspace(np.log10(0.1), np.log10(20.1), 25), extend='max', norm=colors.LogNorm(), vmin=0.02, ax=ax_md0)
      m.contourf(lonGrid, latGrid, rain_dy1, cmap=cms.WhiteBlueGreenYellowRed, levels=np.logspace(np.log10(0.1), np.log10(20.1), 25), extend='max', norm=colors.LogNorm(), vmin=0.02, ax=ax_md1)
      m.contourf(lonGrid, latGrid, rain_dy2, cmap=cms.WhiteBlueGreenYellowRed, levels=np.logspace(np.log10(0.1), np.log10(20.1), 25), extend='max', norm=colors.LogNorm(), vmin=0.02, ax=ax_md2)
  
      f2 = m.contourf(lonGrid, latGrid, rain_dy0-rain_obs, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_dd0)
      m.contourf(lonGrid, latGrid, rain_dy1-rain_obs, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_dd1)
      m.contourf(lonGrid, latGrid, rain_dy2-rain_obs, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_dd2)
      
      m.contour(lonGrid, latGrid, rain_obs, levels=[1, 100], colors='k', linewidths=.5, ax=ax_obs)      
      m.contour(lonGrid, latGrid, rain_obs, levels=[1, 100], colors='k', linewidths=.5, ax=ax_dd0)
      m.contour(lonGrid, latGrid, rain_obs, levels=[1, 100], colors='k', linewidths=.5, ax=ax_dd1)
      m.contour(lonGrid, latGrid, rain_obs, levels=[1, 100], colors='k', linewidths=.5, ax=ax_dd2)
      m.contour(lonGrid, latGrid, rain_dy0, levels=[1, 100], colors='k', linewidths=.5, ax=ax_md0) 
      m.contour(lonGrid, latGrid, rain_dy1, levels=[1, 100], colors='k', linewidths=.5, ax=ax_md1)    
      m.contour(lonGrid, latGrid, rain_dy2, levels=[1, 100], colors='k', linewidths=.5, ax=ax_md2)         
      m.contourf(lonGrid, latGrid, rain_obs, levels=[1, 100], colors='k', alpha=.2, ax=ax_dd0)
      m.contourf(lonGrid, latGrid, rain_obs, levels=[1, 100], colors='k', alpha=.2, ax=ax_dd1)
      m.contourf(lonGrid, latGrid, rain_obs, levels=[1, 100], colors='k', alpha=.2, ax=ax_dd2)
  
      cbar_mm = plt.colorbar(f1, cax=cax_mm, ticks=[0.1, 0.5, 1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20])
      cbar_mm.ax.set_yticklabels([0, 0.5, 1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20])
      cbar_df = plt.colorbar(f2, cax=cax_df, ticks=np.arange(-10, 11, 2))

    if setLabels:
    
      ax_obs.set_title('3-hly RAIN ACC - IMERG-early\n ' + (date_dt-timedelta(hours=3)).strftime('%Y-%m-%d %H - ') + (date_dt).strftime('%Y-%m-%d %H UTC'), fontweight='semibold')
      ax_md0.set_title('UWIN-CM init: ' + date_dt.strftime('%Y-%m-%d 00 UTC'), fontweight='semibold')
      ax_md1.set_title('UWIN-CM init: ' + (date_dt-timedelta(days=1)).strftime('%Y-%m-%d 00 UTC'), fontweight='semibold')
      ax_md2.set_title('UWIN-CM init: ' + (date_dt-timedelta(days=2)).strftime('%Y-%m-%d 00 UTC'), fontweight='semibold')
  
      ax_dd0.set_title('UWIN-CM  -  IMERG-early \n Day 0 forecast; $r^2$=' + '{:.3f}'.format(r2_dy0), fontweight='semibold')
      ax_dd1.set_title('Day 1 forecast; $r^2$=' + '{:.3f}'.format(r2_dy1), fontweight='semibold')
      ax_dd2.set_title('Day 2 forecast; $r^2$=' + '{:.3f}'.format(r2_dy2), fontweight='semibold')
  
      cax_mm.set_title('RR\n[mm/3h]', fontweight='semibold')
      cax_df.set_title(r'$\Delta$RR' + '\n[mm/3h]', fontweight='semibold')
      cax_df.yaxis.set_ticklabels([str(int(num)) for num in np.arange(-10, 11, 2)])
      cax_df.yaxis.set_ticks_position('left')
    plt.savefig(saveDir + 'RAIN_UWINCM_IMERG_latest_01h.png', dpi=200, bbox_inches='tight')
    plt.close()

  if step2_2:
    print(' ... Plotting latest 24-hours summary figure.')
    fls = sorted([fl for fl in os.listdir(dataDir) if '.nc' in fl])
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    fls = [fl for num, fl in enumerate(fls) if (date_dt-timedelta(days=1)) < fl_dates[num] <= date_dt]
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    
    data = Dataset(dataDir + fls[0], 'r')
    lon = np.array(data['lon'])
    lat = np.array(data['lat'])
    data.close()
    lonGrid, latGrid = np.meshgrid(lon, lat)
    
    drain_d0 = []
    drain_d1 = []
    drain_d2 = []
    for fl in fls:
      data = Dataset(dataDir + fl, 'r')
      obs = np.array(data['OBS'])
      drain_d0.append(np.array(data['MOD_d0']) - obs)
      drain_d1.append(np.array(data['MOD_d1']) - obs)
      drain_d2.append(np.array(data['MOD_d2']) - obs)
      data.close()

    drain_d0 = np.ma.masked_invalid(np.stack(drain_d0))
    drain_d1 = np.ma.masked_invalid(np.stack(drain_d1))
    drain_d2 = np.ma.masked_invalid(np.stack(drain_d2))
    
    drain_d0_mean = np.nanmean(drain_d0, 0)
    drain_d1_mean = np.nanmean(drain_d1, 0)
    drain_d2_mean = np.nanmean(drain_d2, 0)

    drain_d0_std = np.nanstd(drain_d0, 0)
    drain_d1_std = np.nanstd(drain_d1, 0)
    drain_d2_std = np.nanstd(drain_d2, 0)


    fig = plt.figure(figsize = (12, 9.5))

    if gsLayout:
      gs = GridSpec(3, 4, width_ratios =[.08, 1, 1, .08])

      ax_md0 = fig.add_subplot(gs[0, 1])
      ax_md1 = fig.add_subplot(gs[1, 1])
      ax_md2 = fig.add_subplot(gs[2, 1])

      ax_sd0 = fig.add_subplot(gs[0, 2])
      ax_sd1 = fig.add_subplot(gs[1, 2])
      ax_sd2 = fig.add_subplot(gs[2, 2])

      cax_md = fig.add_subplot(gs[:, 0])
      cax_sd = fig.add_subplot(gs[:, 3])
  
    if mapFramework:
      plot_mapFramework(m, ax_md0, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md1, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md2, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_sd0)
      plot_mapFramework(m, ax_sd1)
      plot_mapFramework(m, ax_sd2)

    if plotShading:
      f1 = m.contourf(lonGrid, latGrid, drain_d0_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md0)
      m.contourf(lonGrid, latGrid, drain_d1_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md1)
      m.contourf(lonGrid, latGrid, drain_d2_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md2)
  
      f2 = m.contourf(lonGrid, latGrid, drain_d0_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd0, alpha=.8)
      m.contourf(lonGrid, latGrid, drain_d1_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd1, alpha=.8)
      m.contourf(lonGrid, latGrid, drain_d2_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd2, alpha=.8)
  
  
      cbar_md = plt.colorbar(f1, cax=cax_md, ticks=np.arange(-10, 11, 2))
      cbar_sd = plt.colorbar(f2, cax=cax_sd, ticks=np.arange(1, 10.1, 1))

    if setLabels:
      ax_md0.set_title(fl_dates[0].strftime('(%Y-%m-%d %H - ') + fl_dates[-1].strftime(' %Y-%m-%d %H)') + '\nUWIN-CM day 0 forecast - mean bias', fontweight='semibold')
      ax_md1.set_title('UWIN-CM day 1 forecast - mean bias', fontweight='semibold')
      ax_md2.set_title('UWIN-CM day 2 forecast - mean bias', fontweight='semibold')
  

      ax_sd0.set_title('UWIN-CM day 0 forecast - std', fontweight='semibold')
      ax_sd1.set_title('UWIN-CM day 1 forecast - std', fontweight='semibold')
      ax_sd2.set_title('UWIN-CM day 2 forecast - std', fontweight='semibold')
  
      cax_md.set_title('$\Delta$RAIN\n[mm/3h]', fontweight='semibold')
      cax_sd.set_title(r'$\sigma$RAIN' + '\n[mm/3h]', fontweight='semibold')
      cax_md.yaxis.set_ticklabels([str(int(num)) for num in np.arange(-10, 11, 2)])
      cax_sd.yaxis.set_ticks_position('left')

    plt.savefig(saveDir + 'RAIN_UWINCM_IMERG_latest_24h.png', dpi=200, bbox_inches='tight')
    plt.close()
    
  if step2_3:
    print(' ... Plotting latest 48-hours summary figure.')
    fls = sorted([fl for fl in os.listdir(dataDir) if '.nc' in fl])
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    fls = [fl for num, fl in enumerate(fls) if (date_dt-timedelta(days=2)) < fl_dates[num] <= date_dt]
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    
    data = Dataset(dataDir + fls[0], 'r')
    lon = np.array(data['lon'])
    lat = np.array(data['lat'])
    data.close()
    lonGrid, latGrid = np.meshgrid(lon, lat)
    
    drain_d0 = []
    drain_d1 = []
    drain_d2 = []
    for fl in fls:
      data = Dataset(dataDir + fl, 'r')
      obs = np.array(data['OBS'])
      drain_d0.append(np.array(data['MOD_d0']) - obs)
      drain_d1.append(np.array(data['MOD_d1']) - obs)
      drain_d2.append(np.array(data['MOD_d2']) - obs)
      data.close()

    drain_d0 = np.ma.masked_invalid(np.stack(drain_d0))
    drain_d1 = np.ma.masked_invalid(np.stack(drain_d1))
    drain_d2 = np.ma.masked_invalid(np.stack(drain_d2))
    
    drain_d0_mean = np.nanmean(drain_d0, 0)
    drain_d1_mean = np.nanmean(drain_d1, 0)
    drain_d2_mean = np.nanmean(drain_d2, 0)

    drain_d0_std = np.nanstd(drain_d0, 0)
    drain_d1_std = np.nanstd(drain_d1, 0)
    drain_d2_std = np.nanstd(drain_d2, 0)


    fig = plt.figure(figsize = (12, 9.5))

    if gsLayout:
      gs = GridSpec(3, 4, width_ratios =[.08, 1, 1, .08])

      ax_md0 = fig.add_subplot(gs[0, 1])
      ax_md1 = fig.add_subplot(gs[1, 1])
      ax_md2 = fig.add_subplot(gs[2, 1])

      ax_sd0 = fig.add_subplot(gs[0, 2])
      ax_sd1 = fig.add_subplot(gs[1, 2])
      ax_sd2 = fig.add_subplot(gs[2, 2])

      cax_md = fig.add_subplot(gs[:, 0])
      cax_sd = fig.add_subplot(gs[:, 3])
  
    if mapFramework:
      plot_mapFramework(m, ax_md0, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md1, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md2, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_sd0)
      plot_mapFramework(m, ax_sd1)
      plot_mapFramework(m, ax_sd2)

    if plotShading:
      f1 = m.contourf(lonGrid, latGrid, drain_d0_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md0)
      m.contourf(lonGrid, latGrid, drain_d1_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md1)
      m.contourf(lonGrid, latGrid, drain_d2_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md2)
  
      f2 = m.contourf(lonGrid, latGrid, drain_d0_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd0, alpha=.8)
      m.contourf(lonGrid, latGrid, drain_d1_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd1, alpha=.8)
      m.contourf(lonGrid, latGrid, drain_d2_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd2, alpha=.8)
  
  
      cbar_md = plt.colorbar(f1, cax=cax_md, ticks=np.arange(-10, 11, 2))
      cbar_sd = plt.colorbar(f2, cax=cax_sd, ticks=np.arange(1, 10.1, 1))

    if setLabels:
      ax_md0.set_title(fl_dates[0].strftime('(%Y-%m-%d %H - ') + fl_dates[-1].strftime(' %Y-%m-%d %H)') + '\nUWIN-CM day 0 forecast - mean bias', fontweight='semibold')
      ax_md1.set_title('UWIN-CM day 1 forecast - mean bias', fontweight='semibold')
      ax_md2.set_title('UWIN-CM day 2 forecast - mean bias', fontweight='semibold')
  

      ax_sd0.set_title('UWIN-CM day 0 forecast - std', fontweight='semibold')
      ax_sd1.set_title('UWIN-CM day 1 forecast - std', fontweight='semibold')
      ax_sd2.set_title('UWIN-CM day 2 forecast - std', fontweight='semibold')
  
      cax_md.set_title('$\Delta$RAIN\n[mm/3h]', fontweight='semibold')
      cax_sd.set_title(r'$\sigma$RAIN' + '\n[mm/3h]', fontweight='semibold')
      cax_md.yaxis.set_ticklabels([str(int(num)) for num in np.arange(-10, 11, 2)])
      cax_sd.yaxis.set_ticks_position('left')

    plt.savefig(saveDir + 'RAIN_UWINCM_IMERG_latest_48h.png', dpi=200, bbox_inches='tight')
    plt.close()
 
  if step2_4:
    print(' ... Plotting latest 72-hours summary figure.')
    fls = sorted([fl for fl in os.listdir(dataDir) if '.nc' in fl])
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    fls = [fl for num, fl in enumerate(fls) if (date_dt-timedelta(days=3)) < fl_dates[num] <= date_dt]
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    
    data = Dataset(dataDir + fls[0], 'r')
    lon = np.array(data['lon'])
    lat = np.array(data['lat'])
    data.close()
    lonGrid, latGrid = np.meshgrid(lon, lat)
    
    drain_d0 = []
    drain_d1 = []
    drain_d2 = []
    for fl in fls:
      data = Dataset(dataDir + fl, 'r')
      obs = np.array(data['OBS'])
      drain_d0.append(np.array(data['MOD_d0']) - obs)
      drain_d1.append(np.array(data['MOD_d1']) - obs)
      drain_d2.append(np.array(data['MOD_d2']) - obs)
      data.close()

    drain_d0 = np.ma.masked_invalid(np.stack(drain_d0))
    drain_d1 = np.ma.masked_invalid(np.stack(drain_d1))
    drain_d2 = np.ma.masked_invalid(np.stack(drain_d2))
    
    drain_d0_mean = np.nanmean(drain_d0, 0)
    drain_d1_mean = np.nanmean(drain_d1, 0)
    drain_d2_mean = np.nanmean(drain_d2, 0)

    drain_d0_std = np.nanstd(drain_d0, 0)
    drain_d1_std = np.nanstd(drain_d1, 0)
    drain_d2_std = np.nanstd(drain_d2, 0)


    fig = plt.figure(figsize = (12, 9.5))

    if gsLayout:
      gs = GridSpec(3, 4, width_ratios =[.08, 1, 1, .08])

      ax_md0 = fig.add_subplot(gs[0, 1])
      ax_md1 = fig.add_subplot(gs[1, 1])
      ax_md2 = fig.add_subplot(gs[2, 1])

      ax_sd0 = fig.add_subplot(gs[0, 2])
      ax_sd1 = fig.add_subplot(gs[1, 2])
      ax_sd2 = fig.add_subplot(gs[2, 2])

      cax_md = fig.add_subplot(gs[:, 0])
      cax_sd = fig.add_subplot(gs[:, 3])
  
    if mapFramework:
      plot_mapFramework(m, ax_md0, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md1, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md2, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_sd0)
      plot_mapFramework(m, ax_sd1)
      plot_mapFramework(m, ax_sd2)

    if plotShading:
      f1 = m.contourf(lonGrid, latGrid, drain_d0_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md0)
      m.contourf(lonGrid, latGrid, drain_d1_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md1)
      m.contourf(lonGrid, latGrid, drain_d2_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md2)
  
      f2 = m.contourf(lonGrid, latGrid, drain_d0_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd0, alpha=.8)
      m.contourf(lonGrid, latGrid, drain_d1_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd1, alpha=.8)
      m.contourf(lonGrid, latGrid, drain_d2_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd2, alpha=.8)
  
  
      cbar_md = plt.colorbar(f1, cax=cax_md, ticks=np.arange(-10, 11, 2))
      cbar_sd = plt.colorbar(f2, cax=cax_sd, ticks=np.arange(1, 10.1, 1))

    if setLabels:
      ax_md0.set_title(fl_dates[0].strftime('(%Y-%m-%d %H - ') + fl_dates[-1].strftime(' %Y-%m-%d %H)') + '\nUWIN-CM day 0 forecast - mean bias', fontweight='semibold')
      ax_md1.set_title('UWIN-CM day 1 forecast - mean bias', fontweight='semibold')
      ax_md2.set_title('UWIN-CM day 2 forecast - mean bias', fontweight='semibold')
  

      ax_sd0.set_title('UWIN-CM day 0 forecast - std', fontweight='semibold')
      ax_sd1.set_title('UWIN-CM day 1 forecast - std', fontweight='semibold')
      ax_sd2.set_title('UWIN-CM day 2 forecast - std', fontweight='semibold')
  
      cax_md.set_title('$\Delta$RAIN\n[mm/3h]', fontweight='semibold')
      cax_sd.set_title(r'$\sigma$RAIN' + '\n[mm/3h]', fontweight='semibold')
      cax_md.yaxis.set_ticklabels([str(int(num)) for num in np.arange(-10, 11, 2)])
      cax_sd.yaxis.set_ticks_position('left')

    plt.savefig(saveDir + 'RAIN_UWINCM_IMERG_latest_72h.png', dpi=200, bbox_inches='tight')
    plt.close()
 
  if step2_5:
    print(' ... Plotting entire time range summary figure.')
    fls = sorted([fl for fl in os.listdir(dataDir) if '.nc' in fl])
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    fls = [fl for num, fl in enumerate(fls) if record_start < fl_dates[num] <= date_dt]
    fl_dates = [datetime.strptime(fl.split('.')[0].split('_')[-1], '%Y%m%d%H') for fl in fls]
    
    data = Dataset(dataDir + fls[0], 'r')
    lon = np.array(data['lon'])
    lat = np.array(data['lat'])
    data.close()
    lonGrid, latGrid = np.meshgrid(lon, lat)
    
    drain_d0 = []
    drain_d1 = []
    drain_d2 = []
    for fl in fls:
      data = Dataset(dataDir + fl, 'r')
      obs = np.array(data['OBS'])
      drain_d0.append(np.array(data['MOD_d0']) - obs)
      drain_d1.append(np.array(data['MOD_d1']) - obs)
      drain_d2.append(np.array(data['MOD_d2']) - obs)
      data.close()

    drain_d0 = np.ma.masked_invalid(np.stack(drain_d0))
    drain_d1 = np.ma.masked_invalid(np.stack(drain_d1))
    drain_d2 = np.ma.masked_invalid(np.stack(drain_d2))
    
    drain_d0_mean = np.nanmean(drain_d0, 0)
    drain_d1_mean = np.nanmean(drain_d1, 0)
    drain_d2_mean = np.nanmean(drain_d2, 0)

    drain_d0_std = np.nanstd(drain_d0, 0)
    drain_d1_std = np.nanstd(drain_d1, 0)
    drain_d2_std = np.nanstd(drain_d2, 0)


    fig = plt.figure(figsize = (12, 9.5))

    if gsLayout:
      gs = GridSpec(3, 4, width_ratios =[.08, 1, 1, .08])

      ax_md0 = fig.add_subplot(gs[0, 1])
      ax_md1 = fig.add_subplot(gs[1, 1])
      ax_md2 = fig.add_subplot(gs[2, 1])

      ax_sd0 = fig.add_subplot(gs[0, 2])
      ax_sd1 = fig.add_subplot(gs[1, 2])
      ax_sd2 = fig.add_subplot(gs[2, 2])

      cax_md = fig.add_subplot(gs[:, 0])
      cax_sd = fig.add_subplot(gs[:, 3])
  
    if mapFramework:
      plot_mapFramework(m, ax_md0, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md1, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_md2, latLabel=[0,0,0,0])
      plot_mapFramework(m, ax_sd0)
      plot_mapFramework(m, ax_sd1)
      plot_mapFramework(m, ax_sd2)

    if plotShading:
      f1 = m.contourf(lonGrid, latGrid, drain_d0_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md0)
      m.contourf(lonGrid, latGrid, drain_d1_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md1)
      m.contourf(lonGrid, latGrid, drain_d2_mean, levels=np.arange(-10.25, 10.3, .5), cmap='bwr', extend='both', ax=ax_md2)
  
      f2 = m.contourf(lonGrid, latGrid, drain_d0_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd0, alpha=.8)
      m.contourf(lonGrid, latGrid, drain_d1_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd1, alpha=.8)
      m.contourf(lonGrid, latGrid, drain_d2_std, levels=np.arange(1, 10.1, .5), cmap='nipy_spectral', extend='max', ax=ax_sd2, alpha=.8)
  
  
      cbar_md = plt.colorbar(f1, cax=cax_md, ticks=np.arange(-10, 11, 2))
      cbar_sd = plt.colorbar(f2, cax=cax_sd, ticks=np.arange(1, 10.1, 1))

    if setLabels:
      ax_md0.set_title(fl_dates[0].strftime('(%Y-%m-%d %H - ') + fl_dates[-1].strftime(' %Y-%m-%d %H)') + '\nUWIN-CM day 0 forecast - mean bias', fontweight='semibold')
      ax_md1.set_title('UWIN-CM day 1 forecast - mean bias', fontweight='semibold')
      ax_md2.set_title('UWIN-CM day 2 forecast - mean bias', fontweight='semibold')
  

      ax_sd0.set_title('UWIN-CM day 0 forecast - std', fontweight='semibold')
      ax_sd1.set_title('UWIN-CM day 1 forecast - std', fontweight='semibold')
      ax_sd2.set_title('UWIN-CM day 2 forecast - std', fontweight='semibold')
  
      cax_md.set_title('$\Delta$RAIN\n[mm/3h]', fontweight='semibold')
      cax_sd.set_title(r'$\sigma$RAIN' + '\n[mm/3h]', fontweight='semibold')
      cax_md.yaxis.set_ticklabels([str(int(num)) for num in np.arange(-10, 11, 2)])
      cax_sd.yaxis.set_ticks_position('left')

    plt.savefig(saveDir + 'RAIN_UWINCM_IMERG_latest_record.png', dpi=200, bbox_inches='tight')
    plt.close()
 