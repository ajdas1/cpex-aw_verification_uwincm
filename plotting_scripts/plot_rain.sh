#!/bin/bash
source /home/disk/p/asavarin/anaconda3/bin/activate
conda activate base
cd /home/disk/orca/asavarin/2021/test/cpex-aw_verification_uwincm/plotting_scripts

/home/disk/p/asavarin/anaconda3/bin/python /home/disk/orca/asavarin/2021/test/cpex-aw_verification_uwincm/plotting_scripts/plot_rain3hForecastBias_latest.py
