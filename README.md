# cpex-aw_verification_uwincm

Verification of UWIN-CM data for the purposes of CPEX-AW field campaign

----------
### Download MIMIC-TPW data:
No modifications needed. Data is stored in `./data/MIMIC-TPW`

*`python ./processing_scripts/download_mimic-tpw.py YYYYMMDDHH`* 
- where YYYY is the 4-digit year, MM is the 2-digit month, DD is the 2-digit day, and HH is the 2-digit hour. 
- e.g. for 20 July 2021 at 00Z, python ./processing_scripts/download_mimic-tpw.py 2021072000
- if no time is provided in the argument, it defaults to current time.

----------
### Process MIMIC-TPW and model data:
Path modifications needed. Data is read from `./data/MIMIC-TPW` and stored in `./data/MIMIC-model`

**For a single time**:
*`python ./processing_scripts/process_mimic_forecast.py YYYYMMDDHH`*
- where YYYY is the 4-digit year, MM is the 2-digit month, DD is the 2-digit day, and HH is the 2-digit hour. 
- if no time is provided in the argument, it defaults to current time.

**For a time range**:
*`python ./processing_scripts/process_mimic_forecast.py YYYYMMDDHH YYYYMMDDHH`*
- where the first date is the starting point, and the last date is the ending point of the range
- just calls the single-time script multiple times

Modifications (only needed in process_mimic_forecast.py):
- `fcstDir` is the base path to where forecasts are stored
- `day0_fcstDir` is the directory in which the day0 forecast is stored (e.g. initialized at 00Z on given day)  
- `day0_flIn` is the model output file naming convention
- line 79-91: reads in model fields, so might need to change variable names

----------
### Plot model bias for latest time (compared to MIMIC-TPW)

*`python ./plotting_scripts/plot_tpwForecastBias_latest.py`*
- independent of previous scripts

Modifications (might be needed):
- `date_dt` is converted to UTC by adding 7 hours (Pacific time)
- `record_start` is the time when the model started being run daily, so beginning of record
- `fcstDir` is the base path to where forecasts are stored
- `day0_fcstDir` is the directory in which the day0 forecast is stored (e.g. initialized at 00Z on given day)
- `day0_flIn` is the model output file naming convention
- `line 278-293:` panel titles and figure filename
- `line 370-385:` panel titles and figure filename
- `line 462-477:` panel titles and figure filename
- `line 554-569:` panel titles and figure filename
- `line 646-661:` panel titles and figure filename

----------
### Display images in a HTML page
All you need to do is link index.html and ./figs to a server location. 

*`ln -sfv path_to_directory/index.html server_location/cpex_directory/`*

*`ln -sfv path_to_directory/figs server_location/cpex_directory/`*

